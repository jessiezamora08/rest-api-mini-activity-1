const express = require ('express')
const mongoose = require ('mongoose')

const productRoute = require ("./routes/productRoute")
const app = express();
const port = 4001;

app.use(express.json());
app.use(express.urlencoded({extended:true}))

mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.c1f1f.mongodb.net/mini-activity?retryWrites=true&w=majority',
    {
        useNewUrlParser: true, 
        useUnifiedTopology:true,
    }
)

app.use('/products', productRoute)

app.listen(port, () => console.log (`Listening to port ${port}`))