//model
const Product = require('../models/product')

// get all products
module.exports.getAllProducts = () => {
    return Product.find({}).then(result => {
        return result;
    })
}

// get a signle product with Id
module.exports.getProduct = (productId) => {
    return Product.findById(productId).then((result,error) => {
        if(error){
            console.log(error)
            return false
        }else{
            return result;
        }
    })
}

// add new product
module.exports.createProduct = (requestBody) => {
    let newProduct = new Product({
        name: requestBody.name,
        price: requestBody.price
    })

    return newProduct.save().then((product, error) => {
        if(error){
            console.log(error)
        }else{
            return product;
        }
    })
}





module.exports.deleteProduct = (productId) => {
    return Product.findByIdAndRemove(productId).then((removedProduct, err) => {
        if(err){
            console.log(err)
        }else{
            return removedProduct;
        }
    })
}







