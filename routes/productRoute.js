const express = require ('express');
const router = express.Router();

// for import:
const productController = require('../controllers/productController')





// GET ALL PRODUCT

router.get ('/', (req,res) => {
    productController.getAllProducts()
    .then(resultFromController => res.send(resultFromController)) 
})

//CREATE NEW PRODUCT

router.post ('/', (req,res) => {
    productController.createProduct(req.body)
    .then(resultFromController => res.send(resultFromController))
})


//RETRIEVE PRODUCT WHERE Id=Id

router.get('/:id' , (req,res) => {
    productController.getProduct(req.params.id)
    .then(resultFromController => res.send(resultFromController))
    

})

//DELETE PRODUCT WHERE Id=Id

router.delete ('/delete/:id' , (req,res) => {
    productController.deleteProduct(req.params.id)
    .then(resultFromController => res.send(resultFromController))
})





module.exports = router;